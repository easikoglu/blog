package com.easikoglu.blog;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: eerhasi
 * Date: 30.12.2013
 * Time: 22:48
 * To change this template use File | Settings | File Templates.
 */
public class HelloWorld {




   public void visitFiles(File node) {


      if (!node.isDirectory()) {
         System.out.println("File Name : "+ node.getName());
      }
      if (node.isDirectory()) {
         String[] subNote = node.list();
         for (String filename : subNote) {
            visitFiles(new File(node, filename));
         }
      }
   }


   public void visitFiles(String directoryPath) {

       try {
         Path startPath = Paths.get(directoryPath);
         Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {

               try {
                  FileTime lastModifiedTime = Files.getLastModifiedTime(file);
                  Date date = new Date(lastModifiedTime.toMillis());
                  System.out.println("File Name : "+ file.getFileName().toString() + " last modified date :" +date);


               } catch (IOException e) {
                  e.printStackTrace();
               }
               return FileVisitResult.CONTINUE;
            }
         });
      } catch (IOException e) {
         //ignore
      }
   }

   public void visitFiles8(String path){


   }
}
