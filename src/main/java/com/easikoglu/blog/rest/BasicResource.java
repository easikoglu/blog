package com.easikoglu.blog.rest;

import org.apache.commons.lang.CharEncoding;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 * User: easikoglu
 * Date: 30.12.2013
 * Time: 22:49
 */

@Path("/test")
public class BasicResource extends BaseResource{


    @GET
    @Path("ping")
    public String ping() {
        return "server running...";

    }


    @GET
    @Path("pingWithGet")
    public String pingWithGet(@QueryParam("getParam") String getParam) {
        return "server running with " + getParam;
    }


    @POST
    @Path("pingWithPost")
    public String pingWithPost(String postParam) {
        return "server running with " + postParam;
    }

    @POST
    @Path("pingWithRsResponse")
    public Response returnWithRsResponse(String postParam) {
        return Response.ok().entity(postParam).build();
    }


}
