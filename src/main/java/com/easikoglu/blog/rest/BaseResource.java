package com.easikoglu.blog.rest;

import org.apache.commons.lang.CharEncoding;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by eerhasi on 10.01.2014.
 */
@Produces(MediaType. + ";charset=" + CharEncoding.UTF_8)
@Consumes(MediaType.APPLICATION_JSON)
public class BaseResource {

    public BaseResource() {
    }

}
