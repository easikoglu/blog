package com.easikoglu.blog.rest;

import com.easikoglu.blog.api.RequestPojo;
import com.easikoglu.blog.api.ResponsePojo;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created by eerhasi on 10.01.2014.
 */
@Path("json")
public class JsonResource extends BaseResource {


    /**
     *
     * @param request
     * @return
     */
    @POST
    @Path("post")
    public ResponsePojo postJson(RequestPojo request) {
        ResponsePojo response = new ResponsePojo();
        response.setMessage("request key: "+request.getKey() + " request value :"+ request.getValue());
        response.setStatus(Response.Status.OK.getStatusCode());
        return response;
    }

}
