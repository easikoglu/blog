package com.easikoglu.blog.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by easikoglu on 10.01.2014.
 */
@XmlRootElement(name = "request")
public class RequestPojo {

    private Long id;
    private String key;
    private String value;

    public RequestPojo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
