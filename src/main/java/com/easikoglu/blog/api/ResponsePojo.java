package com.easikoglu.blog.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by easikoglu on 10.01.2014.
 */
@XmlRootElement(name = "response")
public class ResponsePojo {


    private String message;
    private int status;

    public ResponsePojo() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
